$(document).ready(function () {
    // var carouselEl = $('.owl-carousel-group');
    // $('.owl-carousel-group').owlCarousel({
    //     loop: true,
    //     margin: 10,
    //     nav: false,
    //     responsive: {
    //         0: {
    //             items: 1
    //         },
    //         600: {
    //             items: 3
    //         },
    //         1000: {
    //             items: 4
    //         }
    //     }
    // })
    // $(".next_group").click(function () {
    //     carouselEl.trigger('next.owl.carousel');
    // });

    // $(".prew_group").click(function () {
    //     carouselEl.trigger('prev.owl.carousel');
    // });

    // var carouselElPersonal = $('.owl-carousel-personal');
    // $('.owl-carousel-personal').owlCarousel({
    //     loop: true,
    //     margin: 10,
    //     nav: false,
    //     responsive: {
    //         0: {
    //             items: 1
    //         },
    //         600: {
    //             items: 3
    //         },
    //         1000: {
    //             items: 4
    //         }
    //     }
    // })
    // $(".next_per").click(function () {
    //     carouselElPersonal.trigger('next.owl.carousel');
    // });

    // $(".prew_per").click(function () {
    //     carouselElPersonal.trigger('prev.owl.carousel');
    // });


    // var carouselElEarth = $('.owl-carousel-earth');
    // carouselElEarth.owlCarousel({
    //     loop: true,
    //     margin: 10,
    //     nav: false,
    //     responsive: {
    //         0: {
    //             items: 3
    //         },
    //         600: {
    //             items: 5
    //         },
    //         1000: {
    //             items: 8
    //         }
    //     }
    // })
    // $(".next_per").click(function () {
    //     carouselElEarth.trigger('next.owl.carousel');
    // });

    // $(".prew_per").click(function () {
    //     carouselElEarth.trigger('prev.owl.carousel');
    // });
    var btn = $('.back_to_top');

    $(window).scroll(function () {
        if ($(window).scrollTop() > 300) {
            btn.addClass('show');
        } else {
            btn.removeClass('show');
        }
    });

    btn.on('click', function (e) {
        e.preventDefault();
        $('html, body').animate({ scrollTop: 0 }, '300');
    });

});

const txtValue = document.querySelectorAll('.txt_value');
txtValue.forEach(e => {
    if (e.offsetWidth > 27 && e.offsetWidth < 37) {
        e.style.fontSize = '13px';
    }
    if (e.offsetWidth > 37) {
        e.style.fontSize = '11px';
    }
})